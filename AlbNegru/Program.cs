﻿using System;

namespace AlbNegru
{
    class Program
    {
        /*
         * Afisati numerele de la 1 la 100.
         * Daca un numar este divizibil cu 3 afisati alb
         * Daca un numar este divizibil cu 5 afisati negru
         * Daca este divizibil si cu 3 si cu 5, afisati alb-negru
         */
        static void Main(string[] args)
        {
            for (int i = 1; i <= 100; i++)
            {
                if (i%3 == 0 && i%5 == 0)
                {
                    Console.WriteLine("alb-negru");
                }
                else if (i%3 == 0)
                {
                    Console.WriteLine("alb");
                }
                else if (i%5 == 0)
                {
                    Console.WriteLine("negru");
                }
                else
                {
                    Console.WriteLine(i);
                }
            }

            Console.ReadKey();
        }
    }
}